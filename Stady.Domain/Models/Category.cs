﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Stady.Domain.Helpers;

namespace Stady.Domain.Models
{
    public class Category:AuditEntities<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateTime { get; set; }

    }
}
