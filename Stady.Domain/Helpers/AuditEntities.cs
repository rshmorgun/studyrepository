﻿using System;

namespace Stady.Domain.Helpers
{
    public class AuditEntities<TKey> where TKey :IEquatable<TKey>
    {
        public TKey Key { get; set; }
    }
}
