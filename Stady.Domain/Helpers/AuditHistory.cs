﻿using System;

namespace Stady.Domain.Helpers
{
   public class AuditHistory
    {
       public DateTime CreatedOn { get; set; }
       public string CreatedBy { get; set; }
    }
}
