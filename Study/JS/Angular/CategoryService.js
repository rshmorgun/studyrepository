﻿var AngularModule = angular.module('app', []);

AngularModule.directive('focusMe', function ($timeout, $parse) {
    return {
        
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                console.log('value=', value);
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
            
            element.bind('blur', function () {
                console.log('blur');
                scope.$apply(model.assign(scope, false));
            });
        }
    };
});

AngularModule.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("enter", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});


AngularModule.controller('getallcategories', function($scope, $http) {
    $http({
        method: 'GET',
        url: 'api/categories/',
        headers: { "Accept": "application/json;odata=verbose" }
    }).success(function(data) {
        $scope.categories = data;
    }).error(function() {
        alert("error");
    });

    $scope.DeleteCategory = function(key) {
        $http.delete('api/categories/delete/' + key).success(function () {

            $scope.categories = $http.get('api/categories/').success(function (data) {
                $scope.categories = data;
                $scope.NewCategoryModel = null;
            }).error(function () {
                alert("error");
            });

        }).error(function () {
            alert("error");
        });
    }
    $scope.CreateNewCategory = function() {
        $http.post('/api/categories/create/', $scope.NewCategoryModel).success(function() {
            $http.get('api/categories/').success(function(data) {
                $scope.categories = data;
                $scope.NewCategoryModel = null;
            }).error(function() { alert("error") });
        });
    }
    $scope.GetCategoryById = function(id) {
        $http.get('/api/categories/' + id)
            .success(function(data) {
                $scope.NewCategoryModel = data;
            }).error(function() {
                alert("error");
            });

    }
});







