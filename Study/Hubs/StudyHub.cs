﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;

namespace Study.Hubs
{
    [HubName("studyHub")]
    public class StudyHub:Hub
    {
        public StudyHub ()
        {
             var serializer = new JsonSerializer()
             {
                 DateTimeZoneHandling = DateTimeZoneHandling.Utc
             };
        GlobalHost.DependencyResolver.Register(typeof (JsonSerializer), () => serializer);
        }

        public void TestSignalr()
        {
            Clients.All.testSignalr();
        }

        public static void SendMessage()
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<StudyHub>();
            hubContext.Clients.All.SendMessege("test signalr");
        }
    }

}