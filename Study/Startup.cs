﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using Study;

//[assembly: OwinStartup(typeof(Study.Startup))]
[assembly: OwinStartupAttribute(typeof(Startup))]
namespace Study
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            var config = new HubConfiguration
            {
                EnableJSONP = true,
                EnableDetailedErrors = true
            };
            app.MapSignalR(config);
        }
    }
}
