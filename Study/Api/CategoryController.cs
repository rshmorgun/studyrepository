﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Stady.Domain.Models;
using Study.Data.Config;
using Study.Data.Repo.Interfaces;
using Study.Data.Repo.Repos;
using Study.Hubs;

namespace Study.Api
{
    public class CategoryController : ApiController
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryController(ICategoryRepository categoryRepository)
        {
             _categoryRepository= categoryRepository
        }

        [Route("api/categories/")]
        [HttpGet]
        public async Task<IHttpActionResult> GetCategories()
        {
            return Ok(await _categoryRepository.Get().ToListAsync());
        }

        [Route("api/categories/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetCategory(Guid id)
        {
            return Ok(await _categoryRepository.Get(x => x.Key == id).FirstOrDefaultAsync());
        }

        [Route("api/categories/create")]
        public async Task<IHttpActionResult> PostCategory(Category model)
        {
            var id = Guid.NewGuid();
            model.Key = id;
            _categoryRepository.Insert(model);
            await _categoryRepository.SaveChangesAsync();
            StudyHub.SendMessage();
            return Ok(model);
        }

        [HttpDelete]
        [Route("api/categories/delete/{id}")]
        public async Task<IHttpActionResult> DeleteCategory(Guid id)
        {
            var category =await _categoryRepository.Get(x => x.Key == id).FirstOrDefaultAsync();
            if (category == null)
            {
                return NotFound();
            }
            _categoryRepository.Delete(category);
            await _categoryRepository.SaveChangesAsync();
            return Ok();
        }
    }
}
