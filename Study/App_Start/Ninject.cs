﻿using Ninject;
using Study.Data.Repo.Interfaces;
using Study.Data.Repo.Repos;

namespace Study.App_Start
{
    public class Ninject
    {
        private readonly IKernel _ninjectKernel;
        public Ninject()
        {
            _ninjectKernel = new StandardKernel();
            AddBindings();
        }

        private void AddBindings()
        {
            _ninjectKernel.Bind<ICategoryRepository>().To<CategoryRepository>();
        }
    }
}