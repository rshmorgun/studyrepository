﻿using System.Data.Entity.ModelConfiguration;
using Study.Data.Models.Users;

namespace Study.Data.Mappings
{
    internal class UserRoleMap : EntityTypeConfiguration<UserRole>
    {
        public UserRoleMap()
        {
            HasKey(x => new { x.UserId, x.RoleId });
            ToTable("UserRoles");
        }
    }
}
