﻿using System.Data.Entity.ModelConfiguration;
using Study.Data.Models.Users;

namespace Study.Data.Mappings
{
    internal class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            HasKey(x => x.Id);
           // HasRequired(x => x.UserName);
            ToTable("Users");
        }
    }
    
}