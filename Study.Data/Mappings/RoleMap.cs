﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Study.Data.Models.Users;

namespace Study.Data.Mappings
{
    internal class RoleMap : EntityTypeConfiguration<Role>
    {
        public RoleMap()
        {
            HasKey(x => x.Id);
            ToTable("Roles");
        }
    }
}
