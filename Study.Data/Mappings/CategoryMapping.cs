﻿using System.Data.Entity.ModelConfiguration;
using Stady.Domain.Models;

namespace Study.Data.Mappings
{
    internal class CategoryMapping:EntityTypeConfiguration<Category>
    {
        public CategoryMapping()
        {
            HasKey(x => x.Key);
        }
    }
}
