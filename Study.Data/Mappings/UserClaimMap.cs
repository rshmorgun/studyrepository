﻿using System.Data.Entity.ModelConfiguration;
using Study.Data.Models.Users;

namespace Study.Data.Mappings
{
    internal class UserClaimMap : EntityTypeConfiguration<UserClaim>
    {
        public UserClaimMap()
        {
            //HasKey(x => x.Id);
            //HasRequired(x => x.User).WithMany().HasForeignKey(x => x.UserId);
            ToTable("UserClaims");
        }
    }
}
