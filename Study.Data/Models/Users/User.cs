﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Study.Data.Config;

namespace Study.Data.Models.Users
{
    public class User : IdentityUser<Guid, UserLogin, UserRole, UserClaim>
    {
        public string Name { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, Guid> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class UserLogin : IdentityUserLogin<Guid>
    {
    }

    public class Role : IdentityRole<Guid, UserRole>
    {
    }

    public class UserRole : IdentityUserRole<Guid>
    {
    }

    public class UserClaim : IdentityUserClaim<Guid>
    {
       // public User User { get; set; }
    }

    public class StudyUserStore : UserStore<User, Role, Guid, UserLogin, UserRole, UserClaim>
    {
        public StudyUserStore(StudyContext context) : base(context)
        {
        }
    }
    
}
