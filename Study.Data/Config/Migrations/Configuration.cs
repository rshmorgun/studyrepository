﻿using System.Data.Entity.Migrations;

namespace Study.Data.Config.Migrations
{
    
        internal sealed class Configuration : DbMigrationsConfiguration<StudyContext>
        {
            public Configuration()
            {
                AutomaticMigrationsEnabled = false;
            }

            protected override void Seed(StudyContext context)
            {
            }
        }
}
