﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Stady.Domain.Models;
using Study.Data.Mappings;
using Study.Data.Models.Users;

namespace Study.Data.Config
{
    public class StudyContext:IdentityDbContext<User, Role, Guid, UserLogin, UserRole, UserClaim>
    {
        static StudyContext()
        {
            Database.SetInitializer<StudyContext>(null);
        }

        public StudyContext()
            : base("Name=StudyContext")
        {
        }

        public static StudyContext Create()

        {
            return new StudyContext();
        }

        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.HasDefaultSchema("dbo");
            //modelBuilder.Configurations.Add(new UserClaimMap());
            modelBuilder.Configurations.Add(new UserMap());
            //modelBuilder.Configurations.Add(new UserRoleMap());
            modelBuilder.Configurations.Add(new RoleMap());
            //modelBuilder.Configurations.Add(new UserLoginMap());
            
            modelBuilder.Configurations.Add(new CategoryMapping());
          
           
        }
    }
}
