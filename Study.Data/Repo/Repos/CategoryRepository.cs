﻿using System.Data.Entity;
using Stady.Domain.Models;
using Study.Data.Repo.Interfaces;

namespace Study.Data.Repo.Repos
{
    public class CategoryRepository:Repository<Category>,ICategoryRepository
    {
        public CategoryRepository(DbContext context) : base(context)
        {
        }
    }
}
