﻿using System;
using System.Collections.Generic;
using System.Linq;
using Stady.Domain.Models;
using Stady.Domain.Repo.Interfaces;

namespace Study.Data.Repo.Interfaces
{
    public interface ICategoryRepository:IRepositoryAsync<Category>
    {
    }
}
